# Task 6 - Movie API

This is a full CRUD API, connected to a PostgreSQL database. Both the API and the database is hosted on Heroku. Project was made using Spring and Hibernate. 

Base-URL for API is https://secure-inlet-82258.herokuapp.com


## Api Documentation

https://documenter.getpostman.com/view/12959028/TVReerAq


## Contributors

- Lars Henrik Skage
- Frida Solheim
- Charlotte Ødegården
