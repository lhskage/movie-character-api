<!-- DATA FOR POPULATING THE DATABASE -->

INSERT INTO character(name, alias, gender, picture_url) VALUES ('Leonardo DiCaprio', 'DiCaprio', 'male', 'dicaprio.png');
INSERT INTO character(name, alias, gender, picture_url) VALUES ('Samuel L. Jackson', '', 'male', 'jackson.png');
INSERT INTO character(name, alias, gender, picture_url) VALUES ('Brad Pitt', '', 'male', 'bradpitt.png');
INSERT INTO character(name, alias, gender, picture_url) VALUES ('Elijah Wood', 'Frodo', 'male', 'elijah.png');

INSERT INTO franchise(franchise_title, description) VALUES ('Quentins 10', 'Quentins 10 movies');
INSERT INTO franchise(franchise_title, description) VALUES ('The Lord of the Rings', 'The Lord of the Rings is a film series of three epic fantasy adventure films directed by Peter Jackson, based on the novel written by J. R. R. Tolkien.');

INSERT INTO movie(director, genre, movie_title, picture_url, release_year, trailer_url, franchise_id) VALUES ('Quentin Tarantino', 'action', 'Django Unchaned', 'django.png', '02.12.2012', 'youtube.com/django', 1);
INSERT INTO movie(director, genre, movie_title, picture_url, release_year, trailer_url, franchise_id) VALUES ('Quentin Tarantino', 'action', 'Once Upon A Time In Hollywood', 'picture.png', '02.12.2012', 'youtube.com/ouatih', 1);
INSERT INTO movie(director, genre, movie_title, picture_url, release_year, trailer_url, franchise_id) VALUES ('Peter Jackson', 'adventure', 'The Lord of the Rings: The Two Towers', 'twotowers.png', '02.12.2002', 'youtube.com/twotowers', 2);

INSERT INTO character_movie(character_id, movie_id) VALUES (1, 1);
INSERT INTO character_movie(character_id, movie_id) VALUES (2, 1);
INSERT INTO character_movie(character_id, movie_id) VALUES (3, 2);
INSERT INTO character_movie(character_id, movie_id) VALUES (4, 3);

<!-- QUERIES TO CHECK THAT RELATIONS ARE WORKING -->
SELECT character.name as charactername, movie.movie_title as movietitle
FROM character
INNER JOIN character_movie
ON character.character_id = character_movie.character_id
INNER JOIN movie
ON character_movie.movie_id = movie.movie_id