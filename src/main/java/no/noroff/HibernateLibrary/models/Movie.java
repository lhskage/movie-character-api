package no.noroff.HibernateLibrary.models;
import com.fasterxml.jackson.annotation.*;
import javax.persistence.*;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
@Entity
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Movie {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(columnDefinition = "serial")
    private Long movieId;
    @Column(name = "movie_title")
    private String movieTitle;
    private String genre;
    @Column(name = "release_year")
    private String releaseYear;
    private String director;
    @Column(name = "picture_url")
    private String pictureUrl;
    @Column(name = "trailer_url")
    private String trailerUrl;
    @ManyToMany()
    @JoinTable(
            name = "character_movie",
            joinColumns = {@JoinColumn(name = "movieId")},
            inverseJoinColumns = {@JoinColumn(name = "characterId")}
    )
    Set<Character> characters;
    @JsonGetter("characters")
    public List<String> characters() {
        if(characters != null) {
            return characters.stream()
                    .map(character -> {
                        return "/api/v1/characters/" + character.getCharacterId();
                    }).collect(Collectors.toList());
        }
        return null;
    }
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "franchise_id", columnDefinition="integer")
    public Franchise franchise;
    @JsonGetter("franchise")
    public String franchise() {
        if(franchise != null){
            return "/api/v1/franchises/" + franchise.getFranchiseId();
        }else{
            return null;
        }
    }
    public Movie() {
    }

    // Getters and setters
    public Long getMovieId() {
        return movieId;
    }
    public void setMovieId(Long movieId) {
        this.movieId = movieId;
    }
    public String getMovieTitle() {
        return movieTitle;
    }
    public void setMovieTitle(String movieTitle) {
        this.movieTitle = movieTitle;
    }
    public String getGenre() {
        return genre;
    }
    public void setGenre(String genre) {
        this.genre = genre;
    }
    public String getReleaseYear() {
        return releaseYear;
    }
    public void setReleaseYear(String releaseYear) {
        this.releaseYear = releaseYear;
    }
    public String getDirector() {
        return director;
    }
    public void setDirector(String director) {
        this.director = director;
    }
    public String getPictureUrl() {
        return pictureUrl;
    }
    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }
    public String getTrailerUrl() {
        return trailerUrl;
    }
    public void setTrailerUrl(String trailerUrl) {
        this.trailerUrl = trailerUrl;
    }
    public Set<Character> getCharacters() {
        return characters;
    }
    public void setCharacters(Set<Character> characters) {
        this.characters = characters;
    }
    public Franchise getFranchise() {
        return franchise;
    }
    public void setFranchise(Franchise franchise) {
        this.franchise = franchise;
    }
}