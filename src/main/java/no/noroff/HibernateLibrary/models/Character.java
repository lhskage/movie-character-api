package no.noroff.HibernateLibrary.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
public class Character {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long characterId;
    private String name;
    private String alias;
    private String gender;

    @Column(name = "picture_url")
    private String pictureUrl;

    @ManyToMany
    @JoinTable(
            name = "Character_Movie",
            joinColumns = {@JoinColumn(name = "characterId")},
            inverseJoinColumns = {@JoinColumn(name = "movieId")}
    )
    List<Movie> movies;

    @JsonGetter("movies")
    public List<String> movies() {
        if(movies != null) {
            return movies.stream()
                .map(movie -> {
                    return "/api/v1/movies/" + movie.getMovieId();
                }).collect(Collectors.toList());
        }
        return null;
    }

    public Character() {
    }

    // Getters and setters
    public Long getCharacterId() {
        return characterId;
    }

    public void setCharacterId(Long characterId) {
        this.characterId = characterId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }
}
