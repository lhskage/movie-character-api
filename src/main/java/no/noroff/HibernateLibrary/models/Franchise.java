package no.noroff.HibernateLibrary.models;

import com.fasterxml.jackson.annotation.*;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Franchise {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long franchiseId;

    @Column(name = "franchise_title")
    private String franchiseTitle;
    @Column(name = "description")
    private String description;

    @OneToMany(mappedBy = "franchise")
    List<Movie> movies;

    @JsonGetter("movies")
    public List<String> movies() {
        if(movies != null) {
            return movies.stream()
                    .map(movie -> {
                        return "/api/v1/movies/" + movie.getMovieId();
                    }).collect(Collectors.toList());
        }
        return null;
    }

    public Franchise() {
    }

    // Getters and setters
    public Long getFranchiseId() {
        return franchiseId;
    }

    public void setFranchiseId(Long franchiseId) {
        this.franchiseId = franchiseId;
    }

    public String getFranchiseTitle() {
        return franchiseTitle;
    }

    public void setFranchiseTitle(String franchiseTitle) {
        this.franchiseTitle = franchiseTitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }
}