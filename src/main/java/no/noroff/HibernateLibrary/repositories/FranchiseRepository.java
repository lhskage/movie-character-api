package no.noroff.HibernateLibrary.repositories;

import no.noroff.HibernateLibrary.models.Franchise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FranchiseRepository extends JpaRepository<Franchise, Long> {

}
