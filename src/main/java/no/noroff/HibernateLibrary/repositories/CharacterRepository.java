package no.noroff.HibernateLibrary.repositories;

import no.noroff.HibernateLibrary.models.Character;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CharacterRepository extends JpaRepository<Character, Long> {
}
