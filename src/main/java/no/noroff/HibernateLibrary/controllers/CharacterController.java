package no.noroff.HibernateLibrary.controllers;

import no.noroff.HibernateLibrary.models.Character;
import no.noroff.HibernateLibrary.models.Movie;
import no.noroff.HibernateLibrary.repositories.CharacterRepository;
import no.noroff.HibernateLibrary.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/characters")
public class CharacterController {
    @Autowired
    private CharacterRepository characterRepository;

    // Get all characters
    @GetMapping()
    public ResponseEntity<List<Character>> getAllCharacters(){
        List<Character> characters = characterRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(characters, status);
    }

    // Get character by id
    @GetMapping("/{id}")
    public ResponseEntity<Character> getCharacter(@PathVariable Long id){
        Character character = new Character();
        HttpStatus status;

        if(characterRepository.existsById(id)){
            status = HttpStatus.OK;
            character = characterRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(character, status);
    }

    // Add a character
    @PostMapping
    public ResponseEntity<Character> addCharacter(@RequestBody Character character) {
        Character addCharacter = characterRepository.save(character);
        HttpStatus status = HttpStatus.CREATED;

        return new ResponseEntity<>(addCharacter, status);
    }

    // Delete character by id
    @DeleteMapping("/{characterId}")
    public void deleteCharacter(@PathVariable Long characterId) {
        if (!characterRepository.existsById(characterId)) {
            System.out.println("Delete didnt work"); // TODO: FIX
        }
        characterRepository.deleteById(characterId);
    }

    // Update character
    @PutMapping("/{characterId}")
    public ResponseEntity<Character> updateAuthor(@PathVariable Long characterId, @RequestBody Character character){
        Character returnCharacter = new Character();
        HttpStatus status;

        if(!characterId.equals(character.getCharacterId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnCharacter,status);
        }
        returnCharacter = characterRepository.save(character);
        status = HttpStatus.OK;
        return new ResponseEntity<>(returnCharacter, status);
    }
}