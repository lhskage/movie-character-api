package no.noroff.HibernateLibrary.controllers;

import no.noroff.HibernateLibrary.models.Character;
import no.noroff.HibernateLibrary.models.Franchise;
import no.noroff.HibernateLibrary.models.Movie;
import no.noroff.HibernateLibrary.repositories.FranchiseRepository;
import no.noroff.HibernateLibrary.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/franchises")
public class FranchiseController {
    @Autowired
    private FranchiseRepository franchiseRepository;

    @Autowired
    private MovieRepository movieRepository;

    // Get all franchises
    @GetMapping()
    public ResponseEntity<List<Franchise>> getAllFranchises(){
        List<Franchise> franchises = franchiseRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(franchises, status);
    }

    // Get a specific franchise
    @GetMapping("/{id}")
    public ResponseEntity<Franchise> getFranchise(@PathVariable Long id){
        Franchise franchise = new Franchise();
        HttpStatus status;

        if(franchiseRepository.existsById(id)){
            status = HttpStatus.OK;
            franchise = franchiseRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(franchise, status);
    }

    // Get all movies for a specific franchise
    @GetMapping("/{id}/movies")
    public ResponseEntity<List<Movie>> getMoviesInFranchise(@PathVariable Long id){
        HttpStatus status;
        List<Movie> correctMovies = new ArrayList<>();
        Franchise franchise;
        if(franchiseRepository.existsById(id)) {
            status = HttpStatus.OK;
            franchise = franchiseRepository.findById(id).get();
            correctMovies = franchise.getMovies();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(correctMovies, status);
    }

    // Get all characters in specific franchise
    @GetMapping("/{id}/characters")
    public ResponseEntity<Set<Character>> getCharactersInFranchise(@PathVariable Long id){
        HttpStatus status;
        Set<Character> correctCharacters = new HashSet<>();

        if(franchiseRepository.existsById(id)) {
            for (Movie movie : franchiseRepository.findById(id).get().getMovies()){
                if (movie.getCharacters() != null && movie.getCharacters().size() != 0){
                    for (Character chara : movie.getCharacters()){
                        if (chara != null){
                            correctCharacters.add(chara);
                        }
                    }
                }
            }
            status = HttpStatus.OK;
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(correctCharacters, status);
    }

    // Add a franchise
    @PostMapping
    public ResponseEntity<Franchise> addFranchise(@RequestBody Franchise franchise) {
        Franchise addFranchise = franchiseRepository.save(franchise);
        HttpStatus status = HttpStatus.CREATED;

        return new ResponseEntity<>(addFranchise, status);
    }

    //Delete a franchise
    @DeleteMapping("/{franchiseId}")
    public void deleteFranchise(@PathVariable Long franchiseId) {
        if (!franchiseRepository.existsById(franchiseId)) {
            System.out.println("Delete didnt work"); // TODO: FIX
        }
        franchiseRepository.deleteById(franchiseId);
    }

    // Update franchise
    @PutMapping("/{franchiseId}")
    public ResponseEntity<Franchise> updateFranchise(@PathVariable Long franchiseId, @RequestBody Franchise franchise){
        Franchise returnFranchise = new Franchise();
        HttpStatus status;

        if(!franchiseId.equals(franchise.getFranchiseId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnFranchise, status);
        }
        returnFranchise = franchiseRepository.save(franchise);
        status = HttpStatus.OK;
        return new ResponseEntity<>(returnFranchise, status);
    }

}