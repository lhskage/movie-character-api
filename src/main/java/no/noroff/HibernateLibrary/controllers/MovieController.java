package no.noroff.HibernateLibrary.controllers;
import no.noroff.HibernateLibrary.models.Character;
import no.noroff.HibernateLibrary.models.Franchise;
import no.noroff.HibernateLibrary.models.Movie;
import no.noroff.HibernateLibrary.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Set;
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/movies")
public class MovieController {
    @Autowired
    private MovieRepository movieRepository;

    // Get all movies
    @GetMapping()
    public ResponseEntity<List<Movie>> getAllMovies(){
        List<Movie> movie = movieRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(movie,status);
    }

    // Get specific movie by id
    @GetMapping("/{id}")
    public ResponseEntity<Movie> getMovieById(@PathVariable Long id){
        Movie movie = new Movie();
        HttpStatus status;
        if(movieRepository.existsById(id)){
            status = HttpStatus.OK;
            movie = movieRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(movie, status);
    }

    // Get all characters in a specific movie
    @GetMapping("/{id}/characters")
    public ResponseEntity<Set<Character>> getCharactersInMovie(@PathVariable Long id){
        HttpStatus status;
        Set<Character> correctCharacters = null;
        Movie movie = new Movie();
        if(movieRepository.existsById(id)) {
            status = HttpStatus.OK;
            movie = movieRepository.findById(id).get();
            correctCharacters = movie.getCharacters();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(correctCharacters, status);
    }

    // Add a new movie to the database
    @PostMapping
    public ResponseEntity<Movie> addMovie(@RequestBody Movie movie) {
        Movie addMovie = movieRepository.save(movie);
        HttpStatus status = HttpStatus.CREATED;

        return new ResponseEntity<>(addMovie, status);
    }

    // Delete movie by id
    @DeleteMapping("/{movieId}")
    public void deleteMovie(@PathVariable Long movieId) {
        if (!movieRepository.existsById(movieId)) {
            System.out.println("Delete didnt work"); // TODO: FIX
        }
        movieRepository.deleteById(movieId);
    }

    // Update specific movie
    @PutMapping("/{movieId}")
    public ResponseEntity<Movie> updateMovie(@PathVariable Long movieId, @RequestBody Movie movie){
        Movie returnMovie = new Movie();
        HttpStatus status;
        if(!movieId.equals(movie.getMovieId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnMovie, status);
        }
        returnMovie = movieRepository.save(movie);
        status = HttpStatus.OK;
        return new ResponseEntity<>(returnMovie, status);
    }
    public MovieRepository getMovieRepository() {
        return movieRepository;
    }
}